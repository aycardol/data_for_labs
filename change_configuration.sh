#!/bin/bash

# Names of environment variables to modify
VAR1_NAME="ROS_MASTER_URI"
VAR2_NAME="ROS_IP"

# Target file to store the modifications
TARGET_FILE="$HOME/.bashrc"

# Check if the target file exists
if [ ! -f "$TARGET_FILE" ]; then
    echo "The file $TARGET_FILE does not exist. Creating..."
    touch "$TARGET_FILE"
fi

# Function to add a source command if not already present
add_source_once() {
    local source_command="$1"
    local file="$2"

    # Check if the source command already exists in the file
    if ! grep -q "^$source_command" "$file"; then
        echo "$source_command" >> "$file"
        echo "The command '$source_command' has been added to the file $file."
    else
        echo "The command '$source_command' already exists in $file."
    fi
}

# Function to add an environment variable
add_variable() {
    local var_name="$1"
    local var_value="$2"
    local file="$3"

    # Check if the variable already exists in the file
    if grep -q "^export $var_name=" "$file"; then
        echo "The variable $var_name already exists."
    else
        # Add the variable
        echo "export $var_name=\"$var_value\"" >> "$file"
        echo "The variable $var_name has been added to the file $file."
    fi
}

# Function to remove an environment variable
remove_variable() {
    local var_name="$1"
    local file="$2"

    # Remove the line corresponding to the variable
    sed -i "/^export $var_name=/d" "$file"
    echo "The variable $var_name has been removed from the file $file."
}

# Function to set a mode message in .bashrc
set_mode_message() {
    local mode="$1"
    local file="$2"
    # Supprimer les lignes existantes pour le mode actuel
    sed -i "/^# Current mode: /d" "$file"
    sed -i "/^echo 'Current mode: /d" "$file"
    
    # Ajouter un commentaire pour le mode actuel
    echo "# Current mode: $mode" >> "$file"
    
    # Ajouter une commande echo pour afficher le mode à chaque ouverture de terminal
    echo "echo 'Current mode: $mode'" >> "$file"
    
    echo "The mode message has been updated to '$mode' in $file."
}


# Add source commands once
add_source_once "source /opt/ros/noetic/setup.bash" "$TARGET_FILE"
add_source_once "source ~/catkin_ws/devel/setup.sh" "$TARGET_FILE"

# Check the provided parameter
if [ "$1" == "local" ]; then
    echo "Local mode selected. Removing variables."
    remove_variable "$VAR1_NAME" "$TARGET_FILE"
    remove_variable "$VAR2_NAME" "$TARGET_FILE"
    set_mode_message "local" "$TARGET_FILE"
elif [ "$1" == "hotspot" ]; then
    echo "Hotspot mode selected. Adding variables."
    add_variable "$VAR1_NAME" "http://10.0.0.42:11311" "$TARGET_FILE"
    add_variable "$VAR2_NAME" "10.0.0.101" "$TARGET_FILE"
    set_mode_message "hotspot" "$TARGET_FILE"
else
    echo "Usage: ~/change_configuration [local|hotspot]"
    exit 1
fi

# Reload the configuration file to apply the changes immediately
source "$TARGET_FILE"

echo "The environment variable changes have been applied."
